Rails.application.routes.draw do
  get 'template', to: 'home#template'
  root 'home#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
